## Projekto 1 dalis

Repozitorijoje pateikti programų tekstai skirti sudaryti statistinius modelius
duomenims, pateiktiems assignment_1/ ir assignment_2 aplankuose.

Statistiniai modeliai sudaromi automatiškai main.py programoje.
Joje automatiškai, pagal pasirinktą duomenų aplanką
(assignment_1 arba assignment_2), atliekamas duomenų paruošimas
(y.txt ir X.txt failų apjungimas, skaidymas į apmokymo,
validavimo ir testavimo duomenų imtis, failų formatų konvertavimas),
statistinių modelių sudarymas ir jų metrikų įvertinimas,
geriausio modelio nustatymas.

Sudarant statistinius modelius ir siekiant išvengti modelių persimokymo,
naudojama Laso L1 ir/arba Tichonovo L2 reguliarizacijos.
Reguliarizacijų parametrai L1 ir L2 nurodyti main.py faile,
tačiau gali būti koreguojami priklausomai nuo poreikio
ir turimų kompiuterinių resursų.

Geriausias modelis parenkamas iškvietus Linux komandas:
python3 init.py 1
arba
python3 init.py 2
atitinkamai pagal tai, kurio duomenų aplanko (1 ar 2) duomenims
sudaromas geriausias modelis.
init.py programa savo ruožtu iškviečia main.py programą,
kuri ir įvykdo aukščiau aprašytas operacijas.
