import sys
import os
import subprocess

# assignment: 1 or 2
assignment = sys.argv[1]

here = os.path.dirname(os.path.abspath(__file__))
path_assignment = os.path.join(here, "data/014",
                               "assignment_{}".format(assignment))

path_data_raw = os.path.join(path_assignment, "data_raw")
path_X = os.path.join(path_data_raw, "X.txt")
path_y = os.path.join(path_data_raw, "y.txt")

path_data_munging = os.path.join(path_assignment, "data_munging")
path_yX_txt = os.path.join(path_data_munging, "yX.txt")
path_yX = os.path.join(path_data_munging, "yX.vw")
path_train_txt = os.path.join(path_data_munging, "train.txt")
path_train = os.path.join(path_data_munging, "train.vw")
path_validation_txt = os.path.join(path_data_munging, "validation.txt")
path_validation = os.path.join(path_data_munging, "validation.vw")
path_test_txt = os.path.join(path_data_munging, "test.txt")
path_test = os.path.join(path_data_munging, "test.vw")

path_vw_readable_models = os.path.join(path_assignment, "vw_readable_models")
path_vw_serialized_models = os.path.join(path_assignment,
                                         "vw_serialized_models")
path_vw_predictions = os.path.join(path_assignment, "vw_predictions")
path_inverted_hashes = os.path.join(path_assignment, "vw_inverted_hashes")
path_metrics = os.path.join(path_assignment, "metrics")

# join y.txt and X.txt data files to yX.txt
cmd_join = " ".join([
    "paste -d ' '",
    path_y,
    path_X,
    "> {}".format(path_yX_txt)
])
subprocess.run(cmd_join, shell=True)

cmd_split = " ".join([
    "mungy",
    "split",
    "--filenames {},{},{}".format(path_train_txt, path_validation_txt,
                                  path_test_txt),
    path_yX_txt,
    "{},{},{}".format(0.6, 0.2, 0.2)
])
subprocess.run(cmd_split, shell=True)


# convert txt data files to vw
def mk_cmd_csv2vw(path_data, path_output):
    cmd = " ".join([
        "mungy",
        "csv2vw",
        path_data,
        "--delim=' '",
        "--label=0",
        "--no-binary-label",
        "--no-header",
        "--output {}".format(path_output)
    ])
    return cmd

subprocess.run(mk_cmd_csv2vw(path_yX_txt, path_yX), shell=True)
subprocess.run(mk_cmd_csv2vw(path_train_txt, path_train), shell=True)
subprocess.run(mk_cmd_csv2vw(path_validation_txt, path_validation), shell=True)
subprocess.run(mk_cmd_csv2vw(path_test_txt, path_test), shell=True)


# train model
def mk_cmd_vw_train(
        path_model, path_readable_model,
        path_data=None, L1=None, L2=None):
    cmd = " ".join([
        "vw",
        "--kill_cache",
        "--normalized",
        "--cache",
        "--holdout_off",
        "--quadratic nn",
        "--loss_function squared",
        "--passes 50",
        "--data {}".format(path_data)
        if path_data and path_data != "-" else "",
        "--final_regressor {}".format(path_model),
        "--readable_model {}".format(path_readable_model),
        "--l1 {}".format(L1) if L1 else "",
        "--l2 {}".format(L2) if L2 else ""
    ])
    return cmd


# predict y
def mk_cmd_vw_predict(
        path_model, path_predictions,
        path_invert_hash, path_data=None):
    cmd = " ".join([
        "vw",
        "--kill_cache",
        "--normalized",
        "--testonly",
        "--data {}".format(path_data)
        if path_data and path_data != "-" else "",
        "--initial_regressor {}".format(path_model),
        "--invert_hash {}".format(path_invert_hash),
        "--predictions {}".format(path_predictions)
    ])
    return cmd

model_parameters = [[0, 0], [0.000001, 0], [0, 0.000001], [0.000001, 0.000001]]
rmses = []

for model_index in range(1, len(model_parameters)+1):
    cmd_train = mk_cmd_vw_train(
        "{}/model_{}.vw_model".format(path_vw_serialized_models, model_index),
        "{}/model_{}.readable_model".format(path_vw_readable_models,
                                            model_index),
        path_train,
        L1=model_parameters[model_index-1][0],
        L2=model_parameters[model_index-1][1])
    subprocess.run(cmd_train, shell=True)

    cmd_predict_1 = mk_cmd_vw_predict(
        "{}/model_{}.vw_model".format(path_vw_serialized_models, model_index),
        "{}/model_{}.txt".format(path_vw_predictions, model_index),
        "{}/model_{}.invert_hash".format(path_inverted_hashes, model_index),
        path_validation)
    subprocess.run(cmd_predict_1, shell=True)

    # metrics
    cmd_join_predictions_1 = " ".join([
        "paste -d ' '",
        path_validation_txt,
        "{}/model_{}.txt".format(path_vw_predictions, model_index),
        "> {}/y__model_{}_predictions.txt".format(path_data_munging,
                                                  model_index)
    ])
    subprocess.run(cmd_join_predictions_1, shell=True)

    cmd_metrics_1_rmse = " ".join([
        "mpipe",
        "rmse",
        path_validation,
        "{}/model_{}.txt".format(path_vw_predictions, model_index),
        "--output {}/model_{}_rmse.txt".format(path_metrics, model_index)
    ])
    subprocess.run(cmd_metrics_1_rmse, shell=True)

    filename = "{}/model_{}_rmse.txt".format(path_metrics, model_index)
    f = open(filename, 'r')
    rmses.append(f.read())

# select best model with the lowest rmse
best_model_index = 0
for i in range(0, len(rmses)):
    if(rmses[i] < rmses[best_model_index]):
        best_model_index = i

# predict
cmd_predict_best = mk_cmd_vw_predict(
        "{}/model_{}.vw_model".format(path_vw_serialized_models,
                                      best_model_index+1),
        "{}/best_model_assignment_{}.txt".format(path_vw_predictions,
                                                 assignment),
        "{}/model_{}.invert_hash".format(path_inverted_hashes,
                                         best_model_index+1),
        path_test)
subprocess.run(cmd_predict_best, shell=True)

# metrics
cmd_join_predictions_best = " ".join([
    "paste -d ' '",
    path_test_txt,
    "{}/best_model_assignment_{}.txt".format(path_vw_predictions, assignment),
    "> {}/y__model_assignment_{}_predictions.txt".format(path_data_munging,
                                                         assignment)
])
subprocess.run(cmd_join_predictions_best, shell=True)

cmd_metrics_rmse = " ".join([
    "mpipe",
    "rmse",
    path_test,
    "{}/best_model_assignment_{}.txt".format(path_vw_predictions, assignment),
    "--output {}/best_model_assignment_{}_rmse.txt".format(path_metrics,
                                                           assignment)
])
subprocess.run(cmd_metrics_rmse, shell=True)
