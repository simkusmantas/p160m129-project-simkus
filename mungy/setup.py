from setuptools import (
    setup,
    find_packages
)

setup(
    name="mungy",
    version="0.1.1",
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        "click==6.7",
        "pandas==0.22",
        "dateutils==0.6.6",
    ],
    entry_points="""
        [console_scripts]
        mungy=mungy.bin.mungy_cli:mungy_cli
    """,
)
